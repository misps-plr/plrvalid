<?php
	
	require 'IClasseModelo.php';
	/**
	* Modelo de classe
	*/
	class ClasseModelo implements IClasseModelo{
		
		private $variavel0;
		
		function __construct($variavel0){

			$this->variavel0 = $variavel0;
			 
		}

		public function getVariavel0(){
			return $this->variavel0;
		}
		
		public function setVariavel0($valor){
			$this->variavel0 = $valor;
		}
		
	}
