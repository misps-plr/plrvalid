
<html>

    <head>
          <meta charset="utf-8">
          <title>PLR - Validação</title>
          <link rel="stylesheet" type="text/css" href="assets/css/mis.css" />
          <link rel="stylesheet" type="text/css" href="assets/css/bsc.css" />
          <link rel="stylesheet" href="assets/css/table.css"/>
          <link rel="stylesheet" href="assets/css/format.circ.css"/>
          <script src="assets/js/jquery-1.11.1.min.js"></script>
          <script src="assets/js/jquery-2.0.1.min.js"></script>
          <script src="assets/js/bsc.js"></script>
          <script src="assets/js/jquery.cookie.js"></script>
  
<style type="text/css">
    div#conteudo-extrato{
        height: 35.8%;
        overflow: auto;
    }
    div#conteudo-ronda{
        height: 30%;
        overflow: auto;
    }
    div#conteudo-ronda{
        height: 30%;
        overflow: auto;
    }

.botao01{
     background: -webkit-linear-gradient(bottom, #E0E0E0, #F9F9F9 70%);
     background: -moz-linear-gradient(bottom, #E0E0E0, #F9F9F9 70%);
     background: -o-linear-gradient(bottom, #E0E0E0, #F9F9F9 70%);
     background: -ms-linear-gradient(bottom, #E0E0E0, #F9F9F9 70%);
     background: linear-gradient(bottom, #E0E0E0, #F9F9F9 70%);
     border: 1px solid #CCCCCE;
     border-radius: 3px;
     box-shadow: 0 3px 0 rgba(0, 0, 0, .3),
                   0 2px 7px rgba(0, 0, 0, 0.2);
     color: #616165;
     display: block;
     font-family: "Trebuchet MS";
     font-size: 14px;
     font-weight: bold;
     line-height: 18px;
     text-align: center;
     text-decoration: none;
     text-transform: uppercase;
     text-shadow:1px 1px 0 #FFF;
     padding: 5px 15px;
     position: relative;
     width: 80px;
     height: 14px;
     float: left;
     margin-top: 11px;
    }
</style>




</style>
  </head>
  <body>
    <div class="user-data">
      <div class="inner">
        <ul>
          <li class="fr logout"><a href="./sso/logout" class="btn-logout"><span class="fr descr-btn">Sair</span></a></li>
          <li class="fr user-meta"><span>Bem vindo(a), </span><span class="bold capitalized user-name"></span></li>
        </ul>
        <div class="clear"></div>
      </div>
    </div>

    <div class="banner">
      <div class="header">
        <div class="inner">
          <div class="fl header-logo">
            <a href="./" title="Ir para p�gina inicial"><img src="./assets/img/logo.png" alt="" /></a>
          </div>
          <div class="fr header-menu">
           
          </div>
          <div class="fr header-breadcrumb" id="breadcrumb">
            <p>
              <ul>



              </ul>
            </p>
          </div>
          <div class="clear"></div>
        </div>
      </div>
      <div class="baseline"></div>
    </div>

        <?php


        require 'src/Form.php';

        $form = new Form('auto','','post');
        $form->newForm();
        echo '<div class="inner">';

        echo '<div class="calendario">';
        echo '<div class="calendario_1">';
        echo "<label>Data inicio:<br>";
        $form->newDate('cldini','2014-01-01','2015-12-31');
        echo "</label>";
        echo "</div>";

        echo '<div class="calendario_2">';
        echo "<label> Data Fim:<br>";
        $form->newDate('cldfim','2014-01-01','2015-12-31');
        echo "</label>";
        echo "</div>";

        echo '<div class="dmatricula">';
        echo "<label> Matricula:<br>";
        $form->newText('txtmatricula','');
        echo "</label>";
        echo "</div>";

        echo '<div class="dtipoajuste">';
        echo "<label> Mês:<br>";
        echo "<select name='cbomes' title='Selecione o mês'>";
        echo "<option value='0'></option>";
        echo "<option value='1'>Jan</option>";
        echo "<option value='2'>Fev</option>";
        echo "<option value='3'>Mar</option>";
        echo "<option value='4'>Abr</option>";
        echo "<option value='5'>Mai</option>";
        echo "<option value='6'>Jun</option>";
        echo "<option value='7'>Jul</option>";
        echo "<option value='8'>Ago</option>";
        echo "<option value='9'>Set</option>";
        echo "<option value='10'>Out</option>";
        echo "<option value='11'>Nov</option>";
        echo "<option value='12'>Dez</option>";
                

        echo "</select>";
        echo "</label>";
        echo "</div>";

        echo '<div class="dtipoajuste">';
        echo "<label> Ajustar:<br>";
        echo "<select name='cboajuste' title='Selecione o indicador a ser ajustado'>";
        echo "<option value='0'></option>";
        echo "<option value='1'>Falta</option>";
        echo "<option value='2'>Qualidade</option>";
        echo "</select>";
        echo "</label>";
        echo "</div>";


        echo '<div class="vajustar">';
        echo "<label> Novo Valor:<br>";
        $form->newText('txtvajustar','');
        echo "</label>";
        echo "</div>";

        echo "<a href='#' name='atualizar' class='botao01'>Atualizar</a>";

        echo '</form>';
        echo '</div>';
        echo '</div>';



     ?>


  

    <!-- Máscara para cobrir a tela -->
  <div 
    id="mask">
  </div>
</div>   


<div class="content">
      <div class="inner">
        <!--Tabela com dados -->
        <H3>Dados Extraidos do Extrato PLR</H3>
        <div id="conteudo-extrato"></div>
        <H3 style='margin-top: 2%;'>Dados Extraidos do Ronda</H3>
        <div id="conteudo-ronda"></div>
        <H3 style='margin-top: 2%;'>Dados Extraidos do Qualidade</H3>
        <div id="conteudo-qualidade"></div>
        <div id="conteudo-update"></div>

      </div>
</div>
    <div class="footer">
        <div class="inner">
          <span>© <?php echo date('Y');?> Porto Seguro - Todos os direitos reservados.</span>
          <span class="fr"><a href="">notas da versão 1.0</a></span>
        </div>
    </div>


<script type="text/javascript">

$(document).ready(function(){
//Validação de Cookies---------------------------------------------------------------------------------------
    $("a[name=atualizar]").click(function(){
        if($("input[name=txtmatricula]").val()!=''){    
            if($("select[name=cbomes]").val()!=0){
                 if($("select[name=cboajuste]").val()!=0){
                     if($("input[name=txtvajustar]").val()!=''){
                         $("#conteudo-update").load("update_plr.php?matricula="+$("input[name=txtmatricula]").val()+"&mes="+$("select[name=cbomes]").val()+"&ajustar="+$("select[name=cboajuste]").val()+"&novovalor="+$("input[name=txtvajustar]").val());
                         alert("Atualizado com sucesso!!!");
                         $("input[name=txtvajustar]").val()=='';
                         $("select[name=cbomes]").val()==0;
                         $("select[name=cboajuste]").val()==0
                     }else{
                         alert("Favor preencher o campo NOVO VALOR.")
                     }
                 }else{
                    alert("Favor selecionar o indicador a ajustar.")
                 }   
            }else{
                alert("Favor selecionar mês.")
            }    
        }else{
                alert("Favor preencher campo matrícula.")
        }  
    });
    
    $("input[name=txtmatricula]").change(function(){
        if($("input[name=txtmatricula]").val()!=''){
            
            $("#conteudo-extrato").html('<p><img src="assets/img/ajax-loader.gif" style="width:50; height:50; position:absolute; left:40%; top:50%;"/></p>')
            $("#conteudo-extrato").load("e_agentes.php?matricula="+$("input[name=txtmatricula]").val()+"&cldini="+$("input[name=cldini]").val()+"&cldfim="+$("input[name=cldfim]").val()), 
            

            $("#conteudo-ronda").html('<p><img src="assets/img/ajax-loader.gif" style="width:50; height:50; position:absolute; left:40%; top:50%;"/></p>')
            $("#conteudo-ronda").load("r_agentes.php?matricula="+$("input[name=txtmatricula]").val()+"&cldini="+$("input[name=cldini]").val()+"&cldfim="+$("input[name=cldfim]").val()),


            $("#conteudo-qualidade").html('<p><img src="assets/img/ajax-loader.gif" style="width:50; height:50; position:absolute; left:40%; top:50%;"/></p>')
            $("#conteudo-qualidade").load("q_agentes.php?matricula="+$("input[name=txtmatricula]").val()+"&cldini="+$("input[name=cldini]").val()+"&cldfim="+$("input[name=cldfim]").val())

        }
    })



});
</script>


  </body>
</html>

