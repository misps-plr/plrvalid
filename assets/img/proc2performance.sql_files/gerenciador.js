function boxAlerta(objTitulo,objMsg){
		var htmlToolTip = new StringBuffer();
		htmlToolTip.append("<table width=\"100%\" border=\"0\" cellspacing=\"2\" cellpadding=\"2\">");
		htmlToolTip.append("<tr>");
		htmlToolTip.append("<td class=\"tooltip\">"+objMsg+"</td>");
		htmlToolTip.append("</tr>");
		htmlToolTip.append("</table>");
	
		Tip( htmlToolTip.toString(),
			TITLE, objTitulo,
			WIDTH, 300,
			SHADOW, false,
			DELAY, 0,
			STICKY, true,
			OFFSETX, 0,
			OFFSETY, 20,
			CLOSEBTN, true,
			CLICKCLOSE, false,
			ABOVE, true);
}
function checaTodos(campo) {
	var ch = false;
	var chSel = document.formDocumentos.chkDocumentoSelecionado;
	var num = chSel.length;
	if(typeof(num) == 'undefined') {
		num = 1;
	}		
	if(campo.checked) {
		ch = true;
	}
	
	if (num==1) { 
		chSel.checked = ch;
		destacarLinha(chSel,chSel.value);
	}else{
		for(var i = 0;i<num;i++) {
			chSel[i].checked = ch;
			destacarLinha(chSel[i],chSel[i].value);
		}
	}
}
function destacarLinha(campo,id) {
	if(campo.checked) {
		document.getElementById(id).style.backgroundColor="#F1F7FD";
	}else{
		document.getElementById(id).style.backgroundColor="#FFFFFF";
	}
}



function findFirstDescendant(parent, tagname)
{
   parent = document.getElementById(parent);
   var descendants = parent.getElementsByTagName(tagname);
   for (i=0;i<descendants.length;i++){
		if(descendants[i].innerHTML == " Erro no ConteudoIframe do Tipo Enquete" ||
		descendants[i].innerHTML == " Servidor portonet.portoseguro.com.br" ||
		descendants[i].innerHTML == " Favor configurar as properties referentes aos ambientes e os links de redirecionamento para o servidor atual. " ||
		descendants[i].innerHTML == " Erro na Url de redirecionamento") {
			descendants[i].innerHTML = "";
		}
   }
}

function addLoadEvent(func) {
    var oldonload = window.onload;
    if (typeof window.onload != 'function') {
        window.onload = func;
    } else {
        window.onload = function() {
            if (oldonload) {
                oldonload();
            }
            func();
        }
    }
}
function limparHs(){
	if(document.location.href.indexOf("https://portonet.portoseguro.com.br/PortoNetExtranet/")!= -1){
	//if(document.location.href.indexOf("C:/Users/f0109915/Desktop/")!= -1){
		findFirstDescendant("geral", "h1");
		findFirstDescendant("geral", "h2");
		findFirstDescendant("geral", "h3");
	}
}



if (window.addEventListener) // W3C standard
{
  window.addEventListener('load', limparHs, false); // NB **not** 'onload'
} 
else if (window.attachEvent) // Microsoft
{
  window.attachEvent('onload', limparHs);
}
/**
FIM SCRIPT
 */