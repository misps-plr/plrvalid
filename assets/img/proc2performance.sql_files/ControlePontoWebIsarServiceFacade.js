
function ControlePontoWebIsarServiceFacade() { }
ControlePontoWebIsarServiceFacade._path = '/controlepontowebisarrh/dwr';

ControlePontoWebIsarServiceFacade.equals = function(p0, callback) {
    DWREngine._execute(ControlePontoWebIsarServiceFacade._path, 'ControlePontoWebIsarServiceFacade', 'equals', p0, callback);
}

ControlePontoWebIsarServiceFacade.hashCode = function(callback) {
    DWREngine._execute(ControlePontoWebIsarServiceFacade._path, 'ControlePontoWebIsarServiceFacade', 'hashCode', callback);
}

ControlePontoWebIsarServiceFacade.toString = function(callback) {
    DWREngine._execute(ControlePontoWebIsarServiceFacade._path, 'ControlePontoWebIsarServiceFacade', 'toString', callback);
}

ControlePontoWebIsarServiceFacade.isExposeProxy = function(callback) {
    DWREngine._execute(ControlePontoWebIsarServiceFacade._path, 'ControlePontoWebIsarServiceFacade', 'isExposeProxy', callback);
}

ControlePontoWebIsarServiceFacade.getProxiedInterfaces = function(callback) {
    DWREngine._execute(ControlePontoWebIsarServiceFacade._path, 'ControlePontoWebIsarServiceFacade', 'getProxiedInterfaces', callback);
}

ControlePontoWebIsarServiceFacade.getTargetSource = function(callback) {
    DWREngine._execute(ControlePontoWebIsarServiceFacade._path, 'ControlePontoWebIsarServiceFacade', 'getTargetSource', callback);
}

ControlePontoWebIsarServiceFacade.toProxyConfigString = function(callback) {
    DWREngine._execute(ControlePontoWebIsarServiceFacade._path, 'ControlePontoWebIsarServiceFacade', 'toProxyConfigString', callback);
}

ControlePontoWebIsarServiceFacade.removeAdvisor = function(p0, callback) {
    DWREngine._execute(ControlePontoWebIsarServiceFacade._path, 'ControlePontoWebIsarServiceFacade', 'removeAdvisor', p0, callback);
}

ControlePontoWebIsarServiceFacade.replaceAdvisor = function(p0, p1, callback) {
    DWREngine._execute(ControlePontoWebIsarServiceFacade._path, 'ControlePontoWebIsarServiceFacade', 'replaceAdvisor', p0, p1, callback);
}

ControlePontoWebIsarServiceFacade.addAdvice = function(p0, callback) {
    DWREngine._execute(ControlePontoWebIsarServiceFacade._path, 'ControlePontoWebIsarServiceFacade', 'addAdvice', p0, callback);
}

ControlePontoWebIsarServiceFacade.isPreFiltered = function(callback) {
    DWREngine._execute(ControlePontoWebIsarServiceFacade._path, 'ControlePontoWebIsarServiceFacade', 'isPreFiltered', callback);
}

ControlePontoWebIsarServiceFacade.addAdvisor = function(p0, callback) {
    DWREngine._execute(ControlePontoWebIsarServiceFacade._path, 'ControlePontoWebIsarServiceFacade', 'addAdvisor', p0, callback);
}

ControlePontoWebIsarServiceFacade.recuperarDataHora = function(callback) {
    DWREngine._execute(ControlePontoWebIsarServiceFacade._path, 'ControlePontoWebIsarServiceFacade', 'recuperarDataHora', false, callback);
}

ControlePontoWebIsarServiceFacade.recuperaDetalheDia = function(p0, p1, callback) {
    DWREngine._execute(ControlePontoWebIsarServiceFacade._path, 'ControlePontoWebIsarServiceFacade', 'recuperaDetalheDia', p0, p1, callback);
}

ControlePontoWebIsarServiceFacade.obterDadosInfoCracha = function(p0, p1, p2, callback) {
    DWREngine._execute(ControlePontoWebIsarServiceFacade._path, 'ControlePontoWebIsarServiceFacade', 'obterDadosInfoCracha', p0, p1, p2, callback);
}

ControlePontoWebIsarServiceFacade.setPreFiltered = function(p0, callback) {
    DWREngine._execute(ControlePontoWebIsarServiceFacade._path, 'ControlePontoWebIsarServiceFacade', 'setPreFiltered', p0, callback);
}

ControlePontoWebIsarServiceFacade.isInterfaceProxied = function(p0, callback) {
    DWREngine._execute(ControlePontoWebIsarServiceFacade._path, 'ControlePontoWebIsarServiceFacade', 'isInterfaceProxied', p0, callback);
}

ControlePontoWebIsarServiceFacade.indexOf = function(p0, callback) {
    DWREngine._execute(ControlePontoWebIsarServiceFacade._path, 'ControlePontoWebIsarServiceFacade', 'indexOf', p0, callback);
}

ControlePontoWebIsarServiceFacade.setTargetSource = function(p0, callback) {
    DWREngine._execute(ControlePontoWebIsarServiceFacade._path, 'ControlePontoWebIsarServiceFacade', 'setTargetSource', p0, callback);
}

ControlePontoWebIsarServiceFacade.isProxyTargetClass = function(callback) {
    DWREngine._execute(ControlePontoWebIsarServiceFacade._path, 'ControlePontoWebIsarServiceFacade', 'isProxyTargetClass', callback);
}

ControlePontoWebIsarServiceFacade.addAdvice = function(p0, p1, callback) {
    DWREngine._execute(ControlePontoWebIsarServiceFacade._path, 'ControlePontoWebIsarServiceFacade', 'addAdvice', p0, p1, callback);
}

ControlePontoWebIsarServiceFacade.recuperarDataHora = function(callback) {
    DWREngine._execute(ControlePontoWebIsarServiceFacade._path, 'ControlePontoWebIsarServiceFacade', 'recuperarDataHora', callback);
}

ControlePontoWebIsarServiceFacade.recuperaInfoColaborador = function(p0, callback) {
    DWREngine._execute(ControlePontoWebIsarServiceFacade._path, 'ControlePontoWebIsarServiceFacade', 'recuperaInfoColaborador', p0, callback);
}

ControlePontoWebIsarServiceFacade.getAdvisors = function(callback) {
    DWREngine._execute(ControlePontoWebIsarServiceFacade._path, 'ControlePontoWebIsarServiceFacade', 'getAdvisors', callback);
}

ControlePontoWebIsarServiceFacade.indexOf = function(p0, callback) {
    DWREngine._execute(ControlePontoWebIsarServiceFacade._path, 'ControlePontoWebIsarServiceFacade', 'indexOf', p0, callback);
}

ControlePontoWebIsarServiceFacade.isFrozen = function(callback) {
    DWREngine._execute(ControlePontoWebIsarServiceFacade._path, 'ControlePontoWebIsarServiceFacade', 'isFrozen', callback);
}

ControlePontoWebIsarServiceFacade.addAdvisor = function(p0, p1, callback) {
    DWREngine._execute(ControlePontoWebIsarServiceFacade._path, 'ControlePontoWebIsarServiceFacade', 'addAdvisor', p0, p1, callback);
}

ControlePontoWebIsarServiceFacade.setExposeProxy = function(p0, callback) {
    DWREngine._execute(ControlePontoWebIsarServiceFacade._path, 'ControlePontoWebIsarServiceFacade', 'setExposeProxy', p0, callback);
}

ControlePontoWebIsarServiceFacade.recuperaListaPonto = function(callback) {
    DWREngine._execute(ControlePontoWebIsarServiceFacade._path, 'ControlePontoWebIsarServiceFacade', 'recuperaListaPonto', false, callback);
}

ControlePontoWebIsarServiceFacade.recuperaListaPonto = function(callback) {
    DWREngine._execute(ControlePontoWebIsarServiceFacade._path, 'ControlePontoWebIsarServiceFacade', 'recuperaListaPonto', callback);
}

ControlePontoWebIsarServiceFacade.getTargetClass = function(callback) {
    DWREngine._execute(ControlePontoWebIsarServiceFacade._path, 'ControlePontoWebIsarServiceFacade', 'getTargetClass', callback);
}

ControlePontoWebIsarServiceFacade.removeAdvice = function(p0, callback) {
    DWREngine._execute(ControlePontoWebIsarServiceFacade._path, 'ControlePontoWebIsarServiceFacade', 'removeAdvice', p0, callback);
}

ControlePontoWebIsarServiceFacade.removeAdvisor = function(p0, callback) {
    DWREngine._execute(ControlePontoWebIsarServiceFacade._path, 'ControlePontoWebIsarServiceFacade', 'removeAdvisor', p0, callback);
}

ControlePontoWebIsarServiceFacade.getProxyClass = function(p0, p1, callback) {
    DWREngine._execute(ControlePontoWebIsarServiceFacade._path, 'ControlePontoWebIsarServiceFacade', 'getProxyClass', p0, p1, callback);
}

ControlePontoWebIsarServiceFacade.newProxyInstance = function(p0, p1, p2, callback) {
    DWREngine._execute(ControlePontoWebIsarServiceFacade._path, 'ControlePontoWebIsarServiceFacade', 'newProxyInstance', p0, p1, p2, callback);
}

ControlePontoWebIsarServiceFacade.isProxyClass = function(p0, callback) {
    DWREngine._execute(ControlePontoWebIsarServiceFacade._path, 'ControlePontoWebIsarServiceFacade', 'isProxyClass', p0, callback);
}

ControlePontoWebIsarServiceFacade.getInvocationHandler = function(p0, callback) {
    DWREngine._execute(ControlePontoWebIsarServiceFacade._path, 'ControlePontoWebIsarServiceFacade', 'getInvocationHandler', p0, callback);
}
