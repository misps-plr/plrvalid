
function RegistraPontoServiceFacade() { }
RegistraPontoServiceFacade._path = '/controlepontowebisarrh/dwr';

RegistraPontoServiceFacade.equals = function(p0, callback) {
    DWREngine._execute(RegistraPontoServiceFacade._path, 'RegistraPontoServiceFacade', 'equals', p0, callback);
}

RegistraPontoServiceFacade.hashCode = function(callback) {
    DWREngine._execute(RegistraPontoServiceFacade._path, 'RegistraPontoServiceFacade', 'hashCode', callback);
}

RegistraPontoServiceFacade.toString = function(callback) {
    DWREngine._execute(RegistraPontoServiceFacade._path, 'RegistraPontoServiceFacade', 'toString', callback);
}

RegistraPontoServiceFacade.isExposeProxy = function(callback) {
    DWREngine._execute(RegistraPontoServiceFacade._path, 'RegistraPontoServiceFacade', 'isExposeProxy', callback);
}

RegistraPontoServiceFacade.getProxiedInterfaces = function(callback) {
    DWREngine._execute(RegistraPontoServiceFacade._path, 'RegistraPontoServiceFacade', 'getProxiedInterfaces', callback);
}

RegistraPontoServiceFacade.getTargetSource = function(callback) {
    DWREngine._execute(RegistraPontoServiceFacade._path, 'RegistraPontoServiceFacade', 'getTargetSource', callback);
}

RegistraPontoServiceFacade.toProxyConfigString = function(callback) {
    DWREngine._execute(RegistraPontoServiceFacade._path, 'RegistraPontoServiceFacade', 'toProxyConfigString', callback);
}

RegistraPontoServiceFacade.removeAdvisor = function(p0, callback) {
    DWREngine._execute(RegistraPontoServiceFacade._path, 'RegistraPontoServiceFacade', 'removeAdvisor', p0, callback);
}

RegistraPontoServiceFacade.replaceAdvisor = function(p0, p1, callback) {
    DWREngine._execute(RegistraPontoServiceFacade._path, 'RegistraPontoServiceFacade', 'replaceAdvisor', p0, p1, callback);
}

RegistraPontoServiceFacade.isPreFiltered = function(callback) {
    DWREngine._execute(RegistraPontoServiceFacade._path, 'RegistraPontoServiceFacade', 'isPreFiltered', callback);
}

RegistraPontoServiceFacade.addAdvice = function(p0, callback) {
    DWREngine._execute(RegistraPontoServiceFacade._path, 'RegistraPontoServiceFacade', 'addAdvice', p0, callback);
}

RegistraPontoServiceFacade.salvarAcessoColaborador = function(p0, callback) {
    DWREngine._execute(RegistraPontoServiceFacade._path, 'RegistraPontoServiceFacade', 'salvarAcessoColaborador', p0, false, callback);
}

RegistraPontoServiceFacade.addAdvisor = function(p0, callback) {
    DWREngine._execute(RegistraPontoServiceFacade._path, 'RegistraPontoServiceFacade', 'addAdvisor', p0, callback);
}

RegistraPontoServiceFacade.setPreFiltered = function(p0, callback) {
    DWREngine._execute(RegistraPontoServiceFacade._path, 'RegistraPontoServiceFacade', 'setPreFiltered', p0, callback);
}

RegistraPontoServiceFacade.isInterfaceProxied = function(p0, callback) {
    DWREngine._execute(RegistraPontoServiceFacade._path, 'RegistraPontoServiceFacade', 'isInterfaceProxied', p0, callback);
}

RegistraPontoServiceFacade.indexOf = function(p0, callback) {
    DWREngine._execute(RegistraPontoServiceFacade._path, 'RegistraPontoServiceFacade', 'indexOf', p0, callback);
}

RegistraPontoServiceFacade.setTargetSource = function(p0, callback) {
    DWREngine._execute(RegistraPontoServiceFacade._path, 'RegistraPontoServiceFacade', 'setTargetSource', p0, callback);
}

RegistraPontoServiceFacade.isProxyTargetClass = function(callback) {
    DWREngine._execute(RegistraPontoServiceFacade._path, 'RegistraPontoServiceFacade', 'isProxyTargetClass', callback);
}

RegistraPontoServiceFacade.addAdvice = function(p0, p1, callback) {
    DWREngine._execute(RegistraPontoServiceFacade._path, 'RegistraPontoServiceFacade', 'addAdvice', p0, p1, callback);
}

RegistraPontoServiceFacade.getAdvisors = function(callback) {
    DWREngine._execute(RegistraPontoServiceFacade._path, 'RegistraPontoServiceFacade', 'getAdvisors', callback);
}

RegistraPontoServiceFacade.isFrozen = function(callback) {
    DWREngine._execute(RegistraPontoServiceFacade._path, 'RegistraPontoServiceFacade', 'isFrozen', callback);
}

RegistraPontoServiceFacade.indexOf = function(p0, callback) {
    DWREngine._execute(RegistraPontoServiceFacade._path, 'RegistraPontoServiceFacade', 'indexOf', p0, callback);
}

RegistraPontoServiceFacade.salvarAcessoColaborador = function(p0, callback) {
    DWREngine._execute(RegistraPontoServiceFacade._path, 'RegistraPontoServiceFacade', 'salvarAcessoColaborador', p0, callback);
}

RegistraPontoServiceFacade.addAdvisor = function(p0, p1, callback) {
    DWREngine._execute(RegistraPontoServiceFacade._path, 'RegistraPontoServiceFacade', 'addAdvisor', p0, p1, callback);
}

RegistraPontoServiceFacade.setExposeProxy = function(p0, callback) {
    DWREngine._execute(RegistraPontoServiceFacade._path, 'RegistraPontoServiceFacade', 'setExposeProxy', p0, callback);
}

RegistraPontoServiceFacade.getTargetClass = function(callback) {
    DWREngine._execute(RegistraPontoServiceFacade._path, 'RegistraPontoServiceFacade', 'getTargetClass', callback);
}

RegistraPontoServiceFacade.removeAdvice = function(p0, callback) {
    DWREngine._execute(RegistraPontoServiceFacade._path, 'RegistraPontoServiceFacade', 'removeAdvice', p0, callback);
}

RegistraPontoServiceFacade.removeAdvisor = function(p0, callback) {
    DWREngine._execute(RegistraPontoServiceFacade._path, 'RegistraPontoServiceFacade', 'removeAdvisor', p0, callback);
}

RegistraPontoServiceFacade.getProxyClass = function(p0, p1, callback) {
    DWREngine._execute(RegistraPontoServiceFacade._path, 'RegistraPontoServiceFacade', 'getProxyClass', p0, p1, callback);
}

RegistraPontoServiceFacade.newProxyInstance = function(p0, p1, p2, callback) {
    DWREngine._execute(RegistraPontoServiceFacade._path, 'RegistraPontoServiceFacade', 'newProxyInstance', p0, p1, p2, callback);
}

RegistraPontoServiceFacade.isProxyClass = function(p0, callback) {
    DWREngine._execute(RegistraPontoServiceFacade._path, 'RegistraPontoServiceFacade', 'isProxyClass', p0, callback);
}

RegistraPontoServiceFacade.getInvocationHandler = function(p0, callback) {
    DWREngine._execute(RegistraPontoServiceFacade._path, 'RegistraPontoServiceFacade', 'getInvocationHandler', p0, callback);
}
